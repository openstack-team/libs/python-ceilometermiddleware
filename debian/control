Source: python-ceilometermiddleware
Section: python
Priority: optional
Maintainer: Debian OpenStack <team+openstack@tracker.debian.org>
Uploaders:
 Thomas Goirand <zigo@debian.org>,
 Mickael Asseline <mickael@papamica.com>,
Build-Depends:
 debhelper-compat (= 11),
 dh-python,
 openstack-pkg-tools,
 python3-all,
 python3-pbr,
 python3-setuptools,
 python3-sphinx,
Build-Depends-Indep:
 python3-babel,
 python3-betamax,
 python3-coverage,
 python3-hacking,
 python3-keystoneauth1 (>= 2.18.0),
 python3-keystoneclient (>= 3.8.0),
 python3-openstackdocstheme (>= 1.17.0),
 python3-oslo.config,
 python3-oslo.context (>= 0.2.0),
 python3-oslo.messaging (>= 5.2.0),
 python3-oslo.utils (>= 3.33.0),
 python3-oslotest (>= 1:1.10.0),
 python3-pycadf,
 python3-subunit,
 python3-swift,
 python3-testscenarios,
 python3-testtools (>= 1.4.0),
 subunit,
 testrepository,
Standards-Version: 4.5.0
Vcs-Browser: https://salsa.debian.org/openstack-team/libs/python-ceilometermiddleware
Vcs-Git: https://salsa.debian.org/openstack-team/libs/python-ceilometermiddleware.git
Homepage: http://www.openstack.org/

Package: python-ceilometermiddleware-doc
Section: doc
Architecture: all
Depends:
 ${misc:Depends},
 ${sphinxdoc:Depends},
Description: OpenStack Telemetry middleware for generating metrics - doc
 This library provides middleware modules designed to enable metric and event
 data generation to be consumed by Ceilometer.
 .
 This package contains the documentation.

Package: python3-ceilometermiddleware
Architecture: all
Depends:
 python3-babel,
 python3-keystoneauth1 (>= 2.18.0),
 python3-keystoneclient (>= 3.8.0),
 python3-oslo.config,
 python3-oslo.messaging (>= 5.2.0),
 python3-oslo.utils (>= 3.33.0),
 python3-pbr,
 python3-pycadf,
 python3-swift,
 ${misc:Depends},
 ${python3:Depends},
Suggests:
 python-ceilometermiddleware-doc,
Description: OpenStack Telemetry middleware for generating metrics - Python 3.x
 This library provides middleware modules designed to enable metric and event
 data generation to be consumed by Ceilometer.
 .
 This package contains the Python 3.x module.
